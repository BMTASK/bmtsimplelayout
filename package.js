/* global Package, Npm */

Package.describe({
  name: 'bmt:bmt-simple-layout',
  summary: 'Simple Layout',
  version: '1.3.1'
});

Npm.depends({
  'lodash': '4.17.4'
});

Package.onUse(function(api) {
  api.use([
    'check',
    'ecmascript',
    'underscore',
    'mongo',
    'blaze',
    'templating',
    'reactive-var',
    'tracker',
    'session',
  ]);

  api.addFiles([
    'client/simpleList/simpleList.html',
    'client/simpleList/styles.html',
  ], ['client']);

  api.mainModule('client/simpleList/simpleList.js', ['client']);

  api.export('modalArray');
});
