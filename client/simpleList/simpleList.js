/* eslint-disable import/no-extraneous-dependencies, import/no-unresolved, import/extensions */
import { Meteor } from 'meteor/meteor'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { Template } from 'meteor/templating'
import { AutoForm } from 'meteor/aldeed:autoform'
import { Materialize } from 'meteor/materialize:materialize'
import { ReactiveDict } from 'meteor/reactive-dict'
import { ReactiveField } from 'meteor/peerlibrary:reactive-field'
import { $ } from 'meteor/jquery'
/* eslint-enable */
import _ from 'lodash'

const modalArray = modalOptions => {
    let af = null
    modalOptions.some(i => {
        if (i.autoForm) {
            af = i
            return true
        }
        return false
    })
    return af
}

function hotkeys (e) {
    if (e.altKey) {
        switch (e.which) {
            case 78: {
                e.preventDefault()
                const btns = this.findAll('.tabular-proyecto-add-btn, .simple-list-add-btn > button')
                btns.some(i => {
                    if (window.isVisible(i)) {
                        i.click()
                        return true
                    }
                    return false
                })
                break
            }
            case 70: {
                e.preventDefault()
                const tables = this.findAll('table.table-proyecto.dataTable')
                tables.some(i => {
                    if (window.isVisible(i)) {
                        const dt = $(i).DataTable()
                        dt.cell({ row: 0, column: 0 }).focus()
                        return true
                    }
                    return false
                })
                break
            }
            case 83: {
                e.preventDefault()
                const searchs = this.findAll('.dataTables_wrapper input[type=search]')
                searchs.some(i => {
                    if (window.isVisible(i)) {
                        i.focus()
                        return true
                    }
                    return false
                })
                break
            }
            // no default
        }
    }
}

function formValidation (form, invalidKeys, validate) {
    invalidKeys.forEach(i => {
        let { name } = i
        if (!form.querySelector(`[data-schema-key="${name}"]`)) {
            name = validate._schemaKeys[validate._schemaKeys.indexOf(name) + 1]
            if (name.indexOf('$') !== -1) {
                const objKey = validate._simpleSchema._objectKeys[name.substr(0, name.indexOf('$') + 2)]
                name = `${name.substr(0, name.indexOf('$'))}0.${objKey[0]}`
            }
        }
        if (form.querySelector(`select[data-schema-key="${name}"]`)) {
            form.querySelector(`select[data-schema-key="${name}"]`).parentElement.querySelector('input').classList.add('proyecto-not-valid-color')
        } else if (form.querySelector(`.proyecto-image-input[data-schema-key="${name}"]`)) {
            form.querySelector(`.proyecto-image-container[name="${name}"]`).classList.add('proyecto-not-valid-color')
        } else if (form.querySelector(`.proyecto-increase-decrease[data-schema-key="${name}"]`)) {
            form.querySelector(`.proyecto-increase-decrease[data-schema-key="${name}"]`).parentElement.classList.add('proyecto-not-valid-color')
        } else if (form.querySelector(`[data-schema-key="${name}"][type=password]`)) {
            form.querySelector(`[data-schema-key="${name}"]`).classList.add('proyecto-not-valid-color')
            form.querySelector(`.autoform-${name}.pass-confirm`).classList.add('proyecto-not-valid-color')
        } else if (form.querySelector(`[data-schema-key="${name}"].content-hide`)) {
            form.querySelector(`[data-schema-key="${name}"].content-hide`).parentElement.querySelector('input.datepicker').classList.add('proyecto-not-valid-color')
        } else if (form.querySelector(`[data-schema-key="${name}"]`)) {
            form.querySelector(`[data-schema-key="${name}"]`).classList.add('proyecto-not-valid-color')
        }
    })
}

function continueForm (instance, formId, form) {
    const validate = AutoForm.getValidationContext(formId),
        invalidKeys = validate._invalidKeys,
        tabular = !instance.data.options.autoForm.tabular ? null : window.findParentContext('Template.tabularTable', document.getElementById(instance.data.options.autoForm.tabular))
    if (!_.isEmpty(invalidKeys)) {
        formValidation(form, invalidKeys, validate)
        Materialize.newToast('Favor de llenar los campos requeridos', 3500, 'toast-negative')
    } else {
        if (tabular) {
            tabular.fakeSelector({ _id: false })
        }
        Meteor.defer(() => {
            if (instance.data.options.autoForm.reset) {
                AutoForm._forceResetFormValues(formId)
                instance.modalReady(false)
                instance.editAutoForm(false)
            }
            if (tabular) {
                tabular.fakeSelector({})
            }
            $(`.${instance.data.options.selector}`).closeModal(_.assignIn({
                dismissible: false,
                complete () {
                    if (tabular) {
                        const dt = $(`#${tabular.data.id}`).DataTable()
                        if (tabular.currentCell) {
                            dt.keys.enable()
                            if (dt.cell(tabular.currentCell()).node()) {
                                dt.cell(tabular.currentCell()).focus()
                            }
                        }
                    }
                    if (instance.data.options.closeComplete) {
                        FlowRouter.current().route.options[instance.data.options.closeComplete].call(instance)
                    }
                }
            }, !instance.data.options.closeOptions ? {} : FlowRouter.current().route.options[instance.data.options.closeOptions].call(instance)))
        })
    }
}

Template.simpleListLayout.onCreated(function create () {
    const instance = this,
        { options = null } = instance.data,
        { events = null } = options
    instance.view.template.__eventMaps[0] = {}
    instance.dict = new ReactiveDict()
    if (options && options.subscribes && options.subscribes.length) {
        const subs = options.subscribes,
            len = subs.length
        let cnt = 0
        instance.subsReady = new ReactiveField(false)
        subs.forEach(i => {
            instance.autorun(() => {
                instance[i.name] = instance.subscribe('simplePublish', i.collection, i.query || {}, i.projection || {}, i.joins || [])
                if (instance[i.name].ready()) {
                    cnt += 1
                    if (cnt === len) {
                        instance.subsReady(true)
                    }
                }
            })
        })
    } else {
        instance.subsReady = new ReactiveField(true)
    }
    if (events) {
        Template.simpleListLayout.events({})
        const map = {}
        events.forEach(i => {
            const key = `${i.name}`
            map[key] = e => {
                e.preventDefault()
                const context = window.findOwnContext(e.target),
                    dat = context.data
                return FlowRouter.current().route.options[i.function].call(dat, e, context)
            }
        })
        _.assignIn(instance.view.template.__eventMaps[0], map)
    }
    if (options && options.functions && options.functions.constructor === Array) {
        options.functions.forEach(i => {
            FlowRouter.current().route.options[i].call(instance)
        })
    }
})

Template.simpleListLayout.onRendered(function render () {
    const instance = this,
        { options = null } = instance.data
    Meteor.defer(() => {
        if (options && options.renderFunctions && options.renderFunctions.constructor === Array) {
            options.renderFunctions.forEach(i => {
                FlowRouter.current().route.options[i].call(instance)
            })
        }
        if (!options.keyTable) {
            document.body.removeEventListener('keydown', hotkeys, false)
        } else {
            document.body.addEventListener('keydown', hotkeys.bind(instance), false)
        }
    })
})

Template.simpleListStyles.onRendered(() => { window.removeExtraStyles('simpleListLayout') })

Template.simpleListContainer.onCreated(function create () {
    const instance = this,
        parent = window.findParentContext('Template.simpleListLayout', instance.view)
    instance.view.template.__eventMaps[0] = {}
    instance.autorun(() => {
        if (parent.subsReady()) {
            const options = instance.data.options || null,
                tabs = !options ? null : options.tabs
            if (options && options.subscribes && options.subscribes.length) {
                const subs = options.subscribes
                subs.forEach(i => {
                    instance[i.name] = instance.subscribe('simplePublish', i.collection, i.query || {}, i.projection || {}, i.joins || [])
                })
            }
            Template.simpleListContainer.events({})
            if (tabs) {
                tabs.forEach(i => {
                    if (i.modalOptions && i.contentOptions && i.contentOptions.cardAdd) {
                        const key = `click .${i.contentOptions.cardAdd}`,
                            modalOptions = i.modalOptions.constructor !== Array ? i.modalOptions : modalArray(i.modalOptions),
                            map = {}
                        map[key] = e => {
                            e.preventDefault()
                            const modal = window.findParentContext('Template.proyectoModal', document.querySelector(`.${modalOptions.selector}`))
                            $(`.${modalOptions.selector}`).openModal(_.assignIn({
                                dismissible: false,
                                ready () {
                                    if (modalOptions.autoForm && modalOptions.autoForm.focus) {
                                        modal.find('.modal input, .modal textarea, .modal select').focus()
                                    }
                                    if (modalOptions.openReady) {
                                        FlowRouter.current().route.options[modalOptions.openReady].call(modal)
                                    }
                                },
                                complete () {
                                    if (modalOptions.openComplete) {
                                        FlowRouter.current().route.options[modalOptions.openComplete].call(modal)
                                    }
                                }
                            }, !modalOptions.openOptions ? {} : FlowRouter.current().route.options[modalOptions.openOptions].call(modal)))
                            modal.editAutoForm(false)
                        }
                        _.assignIn(instance.view.template.__eventMaps[0], map)
                    }
                })
                if (options.reactiveTabs) {
                    instance.reactiveTabs = new ReactiveDict()
                    let currentTab = tabs[0]
                    tabs.forEach(i => {
                        instance.reactiveTabs.set(`${i.tabId}Template`, '')
                        instance.reactiveTabs.set(`${i.tabId}Options`, {})
                    })
                    instance.currentTab = new ReactiveField(tabs[0].tabId)
                    const recursiveTabs = recTabs => {
                        recTabs.forEach(i => {
                            if (i.tabId) {
                                instance.reactiveTabs.set(`${i.tabId}Template`, '')
                                instance.reactiveTabs.set(`${i.tabId}Options`, {})
                                if (i.tabId === instance.currentTab()) {
                                    currentTab = i
                                    if (i.content) {
                                        recursiveTabs(i.content)
                                    }
                                }
                            }
                        })
                    }
                    instance.autorun(() => {
                        if (instance.currentTab()) {
                            let template,
                                templateOptions
                            recursiveTabs(tabs)
                            switch (currentTab.contentType) {
                                case 'cards':
                                    template = 'proyectoCardList'
                                    templateOptions = { options: currentTab }
                                    break
                                case 'table':
                                    template = 'tabularTable'
                                    templateOptions = _.clone(currentTab.contentOptions)
                                    templateOptions.options = _.clone(currentTab)
                                    break
                                case 'multiple':
                                    template = 'simpleListContainerMultiple'
                                    templateOptions = { options: currentTab.contentOptions }
                                    break
                                default:
                                    template = currentTab.contentTemplate
                                    templateOptions = currentTab.contentOptions
                            }
                            instance.reactiveTabs.set(`${instance.currentTab()}Template`, template)
                            instance.reactiveTabs.set(`${instance.currentTab()}Options`, templateOptions)
                        }
                    })
                }
            }
            if (options && options.functions && options.functions.constructor === Array) {
                options.functions.forEach(i => {
                    FlowRouter.current().route.options[i].call(instance)
                })
            }
        }
    })
})

Template.simpleListContainer.onRendered(function render () {
    const instance = this,
        options = instance.data.options || null
    if (instance.find('ul.tabs')) {
        Meteor.defer(() => {
            instance.$('.proyecto-tabs-container ul.proyecto-tabs').tabs()
            const tabs = instance.findAll('.simple-list-content')
            tabs.forEach(i => {
                i.classList.remove('content-hide')
            })
        })
    }
    Meteor.defer(() => {
        if (options && options.renderFunctions && options.renderFunctions.constructor === Array) {
            options.renderFunctions.forEach(i => {
                FlowRouter.current().route.options[i].call(instance)
            })
        }
    })
})

Template.simpleListContent.onCreated(function create () {
    const instance = this,
        { data } = instance,
        { options = null } = data,
        events = (options && options.events) || null
    instance.view.template.__eventMaps[0] = {}
    if (options && options.whenReady) {
        const parent = window.findParentContext('Template.simpleListLayout', instance.view)
        parent[options.whenReady] = new ReactiveField(true)
    }
    if (events) {
        Template.simpleListContent.events({})
        const map = {}
        events.forEach(i => {
            const key = `${i.name}`
            if (!i.modal) {
                map[key] = e => {
                    e.preventDefault()
                    const context = window.findOwnContext(e.target),
                        dat = context.data
                    return FlowRouter.current().route.options[i.function].call(dat, e, context)
                }
            } else {
                map[key] = e => {
                    const context = window.findOwnContext(e.target),
                        modal = window.findOwnContext(document.querySelector(`.${i.modal}`))
                    modal.dataContext(context.data.self)
                    $(`.${modal.data.options.selector}`).openModal(_.assignIn({
                        dismissible: false,
                        ready () {
                            if (modal.data.options && modal.data.options.autoForm && modal.data.options.autoForm.focus) {
                                modal.find('.modal input, .modal textarea, .modal select').focus()
                            }
                            if (modal.data.options.openReady) {
                                FlowRouter.current().route.options[modal.data.options.openReady].call(modal)
                            }
                        },
                        complete () {
                            if (modal.data.options.openComplete) {
                                FlowRouter.current().route.options[modal.data.options.openComplete].call(modal)
                            }
                        }
                    }, !modal.data.options.openOptions ? {} : FlowRouter.current().route.options[modal.data.options.openOptions].call(modal)))
                    Meteor.defer(() => {
                        modal.modalReady(true)
                        if (i.edit) {
                            const _id = context.data.self ? context.data.self._id : context.info()._id
                            const dataContext = context.data.self ? context.data.self : context.info()
                            if(dataContext.useCurrentContext){
                                modal.doc(dataContext)
                            }else{
                                modal.doc(Meteor.Collection.get(modal.data.options.autoForm.collection).findOne({ _id: _id }))
                            }
                            modal.editAutoForm(true)
                        }
                    })
                }
            }
        })
        _.assignIn(instance.view.template.__eventMaps[0], map)
    }
})

Template.proyectoCardList.onCreated(function create () {
    const instance = this,
        { options } = instance.data
    instance.view.template.__eventMaps[0] = {}
    instance.subs = new ReactiveDict()
    instance.content = new ReactiveDict()
    instance.search = new ReactiveDict()
    Template.proyectoCardList.events({})
    if (options.collection) {
        instance.autorun(() => {
            if (!_.isEmpty(options.createQuery)) {
                const { createQuery } = options,
                    serverQuery = createQuery.server || null,
                    clientQuery = createQuery.client || null
                if (serverQuery) {
                    _.assignIn(options.serverQuery, FlowRouter.current().route.options[serverQuery].call(instance))
                }
                if (clientQuery) {
                    _.assignIn(options.query, FlowRouter.current().route.options[clientQuery].call(instance))
                }
            }
            instance.search.set(options.collection, options.query)
            const sub = instance.subscribe('simplePublish', options.collection, options.serverQuery || {}, options.serverProjection || {}, options.joins || [])
            instance.subs.set(options.collection, false)
            if (sub.ready()) {
                instance.content.set(options.collection, Meteor.Collection.get(options.collection).find(instance.search.get(options.collection), options.projection).fetch())
                instance.subs.set(options.collection, true)
            }
        })
    } else {
        console.warn('Favor de ingresar la Collection a utilizar') // eslint-disable-line no-console
    }
    if (options.contentOptions.cardAdd) {
        const key = `click .${options.contentOptions.cardAdd}`,
            map = {}
        let modalOptions = {}
        if (!options.contentOptions.modalAdd) {
            modalOptions = options.modalOptions.constructor !== Array ? options.modalOptions : modalArray(options.modalOptions)
        } else {
            modalOptions = _.find(options.modalOptions, i => i.selector === options.contentOptions.modalAdd)
        }
        map[key] = e => {
            e.preventDefault()
            const modal = window.findParentContext('Template.proyectoModal', document.querySelector(`.${modalOptions.selector}`))
            $(`.${modalOptions.selector}`).openModal(_.assignIn({
                dismissible: false,
                ready () {
                    if (modal.data.options && modal.data.options.autoForm && modal.data.options.autoForm.focus) {
                        modal.find('.modal input, .modal textarea, .modal select').focus()
                    }
                    if (modal.data.options.openReady) {
                        FlowRouter.current().route.options[modal.data.options.openReady].call(modal)
                    }
                },
                complete () {
                    if (modal.data.options.openComplete) {
                        FlowRouter.current().route.options[modal.data.options.openComplete].call(modal)
                    }
                }
            }, !modal.data.options.openOptions ? {} : FlowRouter.current().route.options[modal.data.options.openOptions].call(modal)))
            Meteor.defer(() => {
                $(modal.findAll('select.proyecto-select')).materialSelect({
                    label         : true,
                    labelPosition : 'top'
                })
                modal.editAutoForm(false)
                modal.modalReady(true)
            })
        }
        _.assignIn(instance.view.template.__eventMaps[0], map)
    }
    if (options.contentOptions.cardSearch) {
        const key = `input .${options.contentOptions.cardSearch}`,
            map = {}
        map[key] = _.debounce(e => {
            const val = e.currentTarget.value
            const search = {},
                query = _.clone(options.query)
            if (val !== '') {
                options.searchFields.forEach(i => {
                    search[i] = {
                        $regex   : `.*${val}.*`,
                        $options : 'gi'
                    }
                })
                _.assignIn(query, search)
                instance.search.set(options.collection, query)
            } else {
                instance.search.set(options.collection, options.query)
            }
        }, 250)
        _.assignIn(instance.view.template.__eventMaps[0], map)
    }
    if (options.contentOptions.onHover && !options.contentOptions.onHoverCustomEvent) {
        const key = `click .${options.contentOptions.onHoverEventClass}`,
            modalOptions = options.modalOptions.constructor !== Array ? options.modalOptions : modalArray(options.modalOptions),
            map = {}
        map[key] = e => {
            e.preventDefault()
            const modal = window.findParentContext('Template.proyectoModal', document.querySelector(`.${modalOptions.selector}`)),
                _id = window.findParentContext('Template.cardIconTitulo', e.target).data.id,
                doc = Meteor.Collection.get(options.collection).findOne({ _id })
            modal.doc(doc)
            $(`.${modalOptions.selector}`).openModal(_.assignIn({
                dismissible: false,
                ready () {
                    if (modal.data.options && modal.data.options.autoForm && modal.data.options.autoForm.focus) {
                        modal.find('.modal input, .modal textarea, .modal select').focus()
                    }
                    if (modal.data.options.openReady) {
                        FlowRouter.current().route.options[modal.data.options.openReady].call(modal)
                    }
                },
                complete () {
                    if (modal.data.options.openComplete) {
                        FlowRouter.current().route.options[modal.data.options.openComplete].call(modal)
                    }
                }
            }, !modal.data.options.openOptions ? {} : FlowRouter.current().route.options[modal.data.options.openOptions].call(modal)))
            Meteor.defer(() => {
                $(modal.findAll('select.proyecto-select')).materialSelect({
                    label         : true,
                    labelPosition : 'top'
                })
                modal.editAutoForm(true)
                modal.modalReady(true)
            })
        }
        _.assignIn(instance.view.template.__eventMaps[0], map)
    }
    if (options.contentOptions.cardEventClass) {
        const key = `click .${options.contentOptions.cardEventClass}`,
            map = {}
        if (options.contentOptions.customEvent) {
            map[key] = e => {
                const context = window.findParentContext('Template.cardIconTitulo', e.target)
                return FlowRouter.current().route.options[options.contentOptions.customEvent].call(context.data, e, context)
            }
        } else {
            map[key] = e => {
                e.preventDefault()
                const context = window.findParentContext('Template.cardIconTitulo', e.target).data.self,
                    routeObj = options.contentOptions.routeParams,
                    routeParams = {}
                Object.keys(routeObj).forEach(ky => {
                    if (routeObj[ky] !== 'SELF') {
                        routeParams[ky] = _.get(context, routeObj[ky])
                    } else {
                        routeParams[ky] = FlowRouter.getParam(ky)
                    }
                })
                FlowRouter.go(options.contentOptions.routeName, routeParams)
            }
        }
        _.assignIn(instance.view.template.__eventMaps[0], map)
    }
})

Template.proyectoTabs.onCreated(function create () {
    const instance = this,
        options = instance.data,
        tabs = options.options
    if (options.reactiveTabs) {
        const map = {}
        tabs.forEach(i => {
            const key = `click .${i.selector}`
            map[key] = e => {
                e.preventDefault()
                const tabId = e.target.getAttribute('rel'),
                    temp = window.findParentContext('Template.simpleListContainer', document.getElementById(tabId))
                temp.currentTab(tabId)
            }
        })
        Template.proyectoTabs.events(map)
    }
})

Template.proyectoTabs.onRendered(function render () {
    const instance = this,
        options = instance.data,
        tabs = options.options
    if (tabs.length === 1) {
        instance.find('.proyecto-tabs-container').classList.add('hide-this')
    }
})

Template.proyectoModalStyles.onRendered(() => { window.removeExtraStyles('proyectoModal') })

Template.proyectoModal.onCreated(function create () {
    const instance = this,
        { options } = instance.data,
        { events = null } = options,
        eventsMap = {}
    instance.view.template.__eventMaps[0] = {
        'blur form .proyecto-not-valid-color' (e) {
            e.currentTarget.classList.remove('proyecto-not-valid-color')
        },
        'click form .proyecto-image-container.proyecto-not-valid-color' (e) {
            e.currentTarget.classList.remove('proyecto-not-valid-color')
        },
        'click form .increase-decrease-container.proyecto-not-valid-color' (e) {
            e.currentTarget.classList.remove('proyecto-not-valid-color')
        },
        'keydown .modal.open.key-table' (e) {
            switch (e.which) {
                case 13:
                    e.preventDefault()
                    if (!e.altKey) {
                        const inputs = [],
                            current = e.target,
                            index = inputs.indexOf(current) + 1,
                            len = inputs.length
                        e.target.form.querySelectorAll('input, textarea').forEach(el => {
                            if (window.isVisible(el)) {
                                inputs.push(el)
                            }
                        })
                        if (index < len) {
                            inputs[index].focus()
                        } else {
                            inputs[0].focus()
                        }
                    } else {
                        this.templateInstance().find(`.autoform-${this.templateInstance().data.options.aceptarSelector}`).click()
                    }
                    break
                case 27:
                    e.preventDefault()
                    this.templateInstance().find(`.autoform-${this.templateInstance().data.options.cancelarSelector}`).click()
                    break
                // no default
            }
        },
        'keydown .modal.open.key-table-confirm-cancel' (e) {
            switch (e.which) {
                case 13:
                    e.preventDefault()
                    this.templateInstance().find(`.${this.templateInstance().data.options.aceptarSelector}`).click()
                    break
                case 27:
                    e.preventDefault()
                    this.templateInstance().find(`.${this.templateInstance().data.options.cancelarSelector}`).click()
                    break
                // no default
            }
        },
        'click .js-tabular-clear-search' (e) {
            e.preventDefault()
            const tabular = !this.templateInstance().data.options.autoForm.tabular ? null : window.findParentContext('Template.tabularTable', document.getElementById(this.templateInstance().data.options.autoForm.tabular))
            this.templateInstance().modalReady(false)
            Meteor.defer(() => {
                AutoForm._forceResetFormValues(this.templateInstance().data.options.autoForm.id)
                this.templateInstance().modalReady(true)
                if (this.templateInstance().data.options.clearSearchComplete) {
                    FlowRouter.current().route.options[this.templateInstance().data.options.clearSearchComplete].call(this.templateInstance())
                }
                tabular.selector({ _id: false })
            })
        },
        'click a.js-cancel, click a.js-ok' () {
            if (this.templateInstance().find('.proyecto-modal-content').style.height !== '') {
                Meteor.setTimeout(() => {
                    this.templateInstance().find('.proyecto-modal-content').style.height = ''
                }, 300)
            }
        }
    }
    instance.editAutoForm = new ReactiveField(false)
    instance.doc = new ReactiveField()
    instance.dataContext = new ReactiveField()
    instance.modalReady = new ReactiveField(false)
    if (!options.autoForm) {
        const key = !options.cancelarSelector ? `click .${options.aceptarSelector}` : `click .${options.cancelarSelector}`,
            kye = !options.cancelarSelector ? null : `click .${options.aceptarSelector}`,
            map = {}
        if (kye) {
            map[kye] = e => FlowRouter.current().route.options[instance.data.options.aceptarEvent].call(instance.data, e, instance)
        }
        if (!options.cancelarEvent) {
            map[key] = e => {
                e.preventDefault()
                $(`.${instance.data.options.selector}`).closeModal(_.assignIn({
                    dismissible: false,
                    complete () {
                        if (instance.data.options.closeComplete) {
                            FlowRouter.current().route.options[instance.data.options.closeComplete].call(instance)
                        }
                    }
                }, !instance.data.options.closeOptions ? {} : FlowRouter.current().route.options[instance.data.options.closeOptions].call(instance)))
            }
        } else {
            map[key] = e => {
                e.preventDefault()
                return FlowRouter.current().route.options[instance.data.options.cancelarEvent].call(instance.data, e, instance)
            }
        }
        _.assignIn(eventsMap, map)
    } else {
        if (!_.isEmpty(options.autoForm.fields)) {
            options.autoForm.fields.forEach(i => {
                if (i.constructor === Object) {
                    instance[i.options] = new ReactiveField([i.firstOption])
                }
            })
        }
        if (!_.isEmpty(options.autoForm.updateFields)) {
            options.autoForm.updateFields.forEach(i => {
                if (i.constructor === Object) {
                    instance[i.options] = new ReactiveField([i.firstOption])
                }
            })
        }
        const ke1 = `click .autoform-${options.aceptarSelector}`,
            ke2 = `click .autoform-${options.cancelarSelector}`,
            map = {}
        if (options.aceptarEvent) {
            const ke3 = `click .${options.aceptarEventSelector}`
            map[ke3] = e => FlowRouter.current().route.options[instance.data.options.aceptarEvent].call(instance.data, e, instance)
        }
        map[ke1] = e => {
            e.preventDefault()
            if (instance.data.options.autoForm) {
                const formId = instance.data.options.autoForm.id,
                    form = document.getElementById(formId)
                if (!instance.data.options.autoForm.search) {
                    if (!instance.data.options.autoForm.modify) {
                        instance.find('button[type=submit]').click()
                        continueForm(instance, formId, form)
                    } else if (instance.data.options.autoForm.modify.constructor === Array && instance.data.options.autoForm.modify.length) {
                        instance.data.options.autoForm.modify.forEach(i => {
                            const elem = form.querySelector(`[data-schema-key="${i.field}"]`)
                            FlowRouter.current().route.options[i.name].call(window.findOwnContext(elem), elem)
                        })
                        instance.find('button[type=submit]').click()
                        continueForm(instance, formId, form)
                    }
                } else {
                    const valid = AutoForm.validateForm(formId),
                        validate = AutoForm.getValidationContext(formId),
                        invalidKeys = validate._invalidKeys
                    if (!valid) {
                        formValidation(form, invalidKeys, validate)
                        Materialize.newToast('Favor de seleccionar los campos requeridos', 3500, 'toast-negative')
                    } else {
                        const tabular = !instance.data.options.autoForm.tabular ? null : window.findParentContext('Template.tabularTable', document.getElementById(instance.data.options.autoForm.tabular))
                        tabular.fakeSelector({ _id: false })
                        Meteor.defer(() => {
                            tabular.selector(AutoForm.getFormValues(formId).insertDoc)
                            if (instance.data.options.autoForm.reset) {
                                AutoForm._forceResetFormValues(formId)
                                instance.modalReady(false)
                                instance.editAutoForm(false)
                            }
                            tabular.fakeSelector({})
                            $(`.${instance.data.options.selector}`).closeModal(_.assignIn({
                                dismissible: false,
                                complete () {
                                    if (tabular && tabular.data.tabularOptions.keyTable) {
                                        const dt = $(`#${tabular.data.id}`).DataTable()
                                        dt.keys.enable()
                                        if (dt.cell(tabular.currentCell()).node()) {
                                            dt.cell(tabular.currentCell()).focus()
                                        }
                                    }
                                    if (instance.data.options.closeComplete) {
                                        FlowRouter.current().route.options[instance.data.options.closeComplete].call(instance)
                                    }
                                }
                            }, !instance.data.options.closeOptions ? {} : FlowRouter.current().route.options[instance.data.options.closeOptions].call(instance)))
                        })
                    }
                }
            }
        }
        map[ke2] = e => {
            e.preventDefault()
            const tabular = !instance.data.options.autoForm.tabular ? null : window.findParentContext('Template.tabularTable', document.getElementById(instance.data.options.autoForm.tabular))
            Meteor.defer(() => {
                AutoForm._forceResetFormValues(instance.data.options.autoForm.id)
                instance.modalReady(false)
                instance.editAutoForm(false)
            })
            $(`.${instance.data.options.selector}`).closeModal(_.assignIn({
                dismissible: false,
                complete () {
                    if (tabular && tabular.data.tabularOptions.keyTable) {
                        const dt = $(`#${tabular.data.id}`).DataTable()
                        dt.keys.enable()
                        if (dt.cell(tabular.currentCell()).node()) {
                            dt.cell(tabular.currentCell()).focus()
                        }
                    }
                    if (instance.data.options.closeComplete) {
                        FlowRouter.current().route.options[instance.data.options.closeComplete].call(instance)
                    }
                }
            }, !instance.data.options.closeOptions ? {} : FlowRouter.current().route.options[instance.data.options.closeOptions].call(instance)))
        }
        _.assignIn(eventsMap, map)
    }
    if (options && options.functions && options.functions.constructor === Array) {
        options.functions.forEach(i => {
            FlowRouter.current().route.options[i].call(instance, instance)
        })
    }
    if (events) {
        const map = {}
        events.forEach(i => {
            const key = `${i.name}`
            map[key] = e => {
                e.preventDefault()
                const context = window.findOwnContext(e.target),
                    dat = context.data
                return FlowRouter.current().route.options[i.function].call(dat, e, context)
            }
        })
        _.assignIn(eventsMap, map)
    }
    _.assignIn(instance.view.template.__eventMaps[0], eventsMap)
})

Template.proyectoModal.onRendered(function render () {
    const instance = this,
        { options } = instance.data
    Meteor.defer(() => {
        if (options && options.renderFunctions && options.renderFunctions.constructor === Array) {
            options.renderFunctions.forEach(i => {
                FlowRouter.current().route.options[i].call(instance)
            })
        }
        if (instance.keyTable) {
            instance.find('.modal').classList.add('key-table')
        }
        if (options.keyTable) {
            instance.find('.modal').classList.add('key-table-confirm-cancel')
            instance.find('.modal').setAttribute('tabindex', -1)
        }
    })
})

Template.proyectoModal.onDestroyed(() => {
    const arr = document.querySelectorAll('.lean-overlay')
    if (arr) {
        arr.forEach(i => {
            i.remove()
        })
    }
})

export default { modalArray }
